FROM docker.elastic.co/logstash/logstash:7.13.1
WORKDIR /pipeline
COPY ./logstash.conf .
CMD ["logstash", "-f", "logstash.conf"]
# CMD "logstash"