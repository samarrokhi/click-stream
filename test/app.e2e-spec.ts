import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });

  it('/track (post)', () => {
    return request(app.getHttpServer())
      .post('/track')
      .send({
        eventType: "buttonclick",
        eventSource: "btn_submit",
        userId: "adf84db5-9c28-4450-ac1b-b98aa784afc2"
      })
      .expect(200)
      .expect({
        succeed: true
      });
  });
});
