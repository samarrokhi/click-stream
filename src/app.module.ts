import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TrackModule } from './tracking/track.module';

@Module({
  imports: [ConfigModule.forRoot({
    isGlobal: true,
  }), TrackModule]
})
export class AppModule {}
