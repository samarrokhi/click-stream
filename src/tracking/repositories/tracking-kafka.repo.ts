import { Kafka, Producer } from "kafkajs";
import { TrackingModelInterface } from "../models/tracking.model";
import { ConfigService } from "@nestjs/config"
import { Inject, Injectable } from "@nestjs/common";

export interface TrackingRepoInterface {
    publish(tracking: TrackingModelInterface): Promise<boolean>
}

@Injectable()
export class TrackingKafkaRepo implements TrackingRepoInterface {

    private readonly kafka: Kafka;
    private readonly producer: Producer;

    constructor(private configService: ConfigService) {
        this.kafka = new Kafka({
            clientId: 'my-app',
            brokers: [this.configService.get<string>('KAFKA_BROKERS')]
        });

        this.producer = this.kafka.producer();
    }

    async publish(tracking: TrackingModelInterface): Promise<boolean> {
        await this.producer.connect();
        const record = await this.producer.send({ topic: "click-events", messages: [{ value: JSON.stringify(tracking) }] });
        return record[0].errorCode === 0;
    }
}