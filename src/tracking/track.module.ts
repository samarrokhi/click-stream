import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TrackingKafkaRepo } from './repositories/tracking-kafka.repo';
import { TrackService } from './services/track.service';
import { TrackController } from './track.controller';

@Module({
  controllers: [TrackController],
  imports: [ConfigModule],
  providers: [{ provide: 'TrackServiceInterface', useClass: TrackService },
  { provide: 'TrackingRepoInterface', useClass: TrackingKafkaRepo }],
})
export class TrackModule { }
