import { Body, Controller, Inject, Post } from '@nestjs/common';
import { TrackingRequestModel } from './models/tracking-req.model';
import { TrackingResponseModel } from './models/tracking-res.model';
import { TrackServiceInterface } from './services/track.service';

@Controller('track')
export class TrackController {
    constructor(@Inject('TrackServiceInterface') private readonly trackService: TrackServiceInterface) {

    }
    @Post()
    async track(@Body() trackDto: TrackingRequestModel): Promise<TrackingResponseModel> {
        return { succeed: await this.trackService.publish(trackDto) };
    }
}

