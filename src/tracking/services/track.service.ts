import { Inject } from "@nestjs/common";
import { TrackingRequestModel } from "../models/tracking-req.model";
import { TrackingRepoInterface } from "../repositories/tracking-kafka.repo";
import * as moment from "moment";

export interface TrackServiceInterface {
    publish(tracking: TrackingRequestModel): Promise<boolean>;
}

export class TrackService implements TrackServiceInterface {

    constructor(@Inject("TrackingRepoInterface") private readonly trackingRepo: TrackingRepoInterface) {

    }

    async publish(tracking: TrackingRequestModel): Promise<boolean> {
        const now = moment();
        return this.trackingRepo.publish({
            eventSource: tracking.eventSource,
            eventType: tracking.eventType,
            userId: tracking.userId,
            timestamp: now.unix(),
            utcDatetime: moment(now).utc().format()
        });
    }

}