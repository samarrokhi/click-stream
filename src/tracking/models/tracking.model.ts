export interface TrackingModelInterface {
    eventType: string;
    eventSource: string;
    userId: string;
    timestamp: number;
    utcDatetime: string;
}