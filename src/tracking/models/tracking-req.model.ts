import { IsEnum, IsOptional, IsString, IsUUID } from "class-validator";
import { EventType } from "../enums/eventType";

export class TrackingRequestModel {
    @IsEnum(EventType)
    eventType: string;
    
    @IsString()
    eventSource: string;

    @IsUUID()
    userId: string;
}
