import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TrackingKafkaRepo } from './repositories/tracking-kafka.repo';
import { TrackService } from './services/track.service';
import { TrackController } from './track.controller';

describe('TrackController', () => {
  let controller: TrackController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TrackController],
      imports: [ConfigModule],
      providers: [{ provide: 'TrackServiceInterface', useClass: TrackService },
      { provide: 'TrackingRepoInterface', useClass: TrackingKafkaRepo }],
    }).compile();

    controller = module.get<TrackController>(TrackController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
